package com.hackus.learn.facebook.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacebookAuthApp {

    public static void main(String[] args) {
        SpringApplication.run(FacebookAuthApp.class, args);
    }
}
