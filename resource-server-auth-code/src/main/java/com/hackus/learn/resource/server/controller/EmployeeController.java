package com.hackus.learn.resource.server.controller;

import org.apache.commons.codec.binary.Base64;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.hackus.learn.domain.ResponseGetEmployee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequiredArgsConstructor
public class EmployeeController {

//    @RequestMapping(value = "/user/getEmployeesList", method = RequestMethod.GET, produces = "application/json")
//    @ResponseBody
//    public ResponseEntity<List<ResponseGetEmployee>> getEmployeesList() {
//        List<ResponseGetEmployee> employees = new ArrayList<>();
//        ResponseGetEmployee emp = ResponseGetEmployee.builder()
//            .empId("emp1")
//            .empName("emp1")
//            .build();
//        employees.add(emp);
//        return ResponseEntity.ok()
//            .body(employees);
//    }

    @GetMapping(value = "/user/getEmployeesList", produces = "application/json")
    public List<ResponseGetEmployee> getEmployeesList() {
        List<ResponseGetEmployee> employees = new ArrayList<>();
        ResponseGetEmployee emp = ResponseGetEmployee.builder()
            .empId("emp1")
            .empName("emp1")
            .build();
        employees.add(emp);
        return employees;
    }
}
