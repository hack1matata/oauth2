package com.hackus.learn.client.config;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
//
//    @Bean
//    public RestOperations restTemplateAuth(
//        OAuth2ProtectedResourceDetails oAuth2ProtectedResourceDetails,
//        OAuth2ClientContext oAuth2ClientContext) {
//        return new OAuth2RestTemplate(oAuth2ProtectedResourceDetails, oAuth2ClientContext);
//    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
