package com.hackus.learn.client.config;

import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;

@Configuration
@EnableOAuth2Sso
//@EnableWebSecurity
//@Order(value = 0)
//@EnableOAuth2Client
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
//            .anonymous().disable()
            .authorizeRequests()
            .antMatchers("/").permitAll()
            .and()
            .csrf().disable();
    }

//    @Bean
//    public AuthorizationCodeResourceDetails azureDetails() {
//        AuthorizationCodeResourceDetails details = new AuthorizationCodeResourceDetails();
//        details.setClientId("myclientId");
//        details.setClientSecret("myclientsecret");
//        details.setAccessTokenUri("https://login.microsoftonline.com/common/oauth2/token");
//        details.setUserAuthorizationUri("https://login.microsoftonline.com/common/oauth2/authorize");
//        details.setScope(Arrays.asList("openid","profile","User.Read","Calendars.Read","Chat.Read","Files.Read",
//        "Mail.Read","Notes.Read","Tasks.Read"));
//        return details;
//    }

//    @Bean
//    public OAuth2RestTemplate oauth2RestTemplate(OAuth2ClientContext oauth2ClientContext) {
//        return new OAuth2RestTemplate(azureDetails(),oauth2ClientContext);
//    }

    @Bean
    public OAuth2RestTemplate oauth2RestTemplate(
        OAuth2ClientContext oauth2ClientContext,
        OAuth2ProtectedResourceDetails details
//        AuthorizationCodeResourceDetails details
    ) {
        return new OAuth2RestTemplate(details, oauth2ClientContext);
    }
}
