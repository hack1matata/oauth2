package com.hackus.learn.client.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackus.learn.domain.ResponseGetEmployee;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Arrays;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@RestController
@RequiredArgsConstructor
@Slf4j
public class EmployeeController {

    private final RestTemplate restTemplate;
    //    private final RestOperations restTemplateAuth;
//    private final OAuth2RestTemplate oauth2RestTemplate;
    private final ObjectMapper objectMapper;

    @RequestMapping(value = "/getEmployees",
        method = RequestMethod.GET)
    public ModelAndView getEmployeeInfo(Model model) {
        ModelAndView modelAndView = new ModelAndView("getEmployees");
        return modelAndView;
    }

    @RequestMapping(value = "/showEmployees",
        method = RequestMethod.GET)
    public ModelAndView showEmployees(@RequestParam("code") String code) throws JsonProcessingException, IOException {
        System.out.println("Authorization code------" + code);

        String credentials = "javainuse:secret";
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Authorization", "Basic " + encodedCredentials);

        HttpEntity<String> request = new HttpEntity<String>(headers);

        String access_token_url = "http://localhost:8082/auth/oauth/token";
        access_token_url += "?code=" + code;
        access_token_url += "&grant_type=authorization_code";
        access_token_url += "&redirect_uri=http://localhost:8083/client/showEmployees";

        ResponseEntity<String> response = restTemplate.exchange(
            access_token_url,
            HttpMethod.POST,
            request,
            String.class
        );

        log.info("Access Token Response ---------" + response.getBody());

        // Get the Access Token From the received JSON response
        JsonNode node = objectMapper.readTree(response.getBody());
        String token = node.path("access_token").asText();

        String url = "http://localhost:8081/resource/user/getEmployeesList";

        log.info("Access token: [{}]", token);
        // Use the access token for authentication
        HttpHeaders headers1 = new HttpHeaders();
//        headers1.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers1.add("Authorization", "Bearer " + token);
//        headers1.add("grant_type", "refresh_token");
        HttpEntity<String> entity = new HttpEntity<>(headers1);

        ResponseEntity<ResponseGetEmployee[]> employees = restTemplate.getForEntity(
            url,
            ResponseGetEmployee[].class,
            entity
        );
        System.out.println(employees);
        ResponseGetEmployee[] employeeArray = employees.getBody();

        ModelAndView model = new ModelAndView("showEmployees");
        model.addObject("employees", Arrays.asList(employeeArray));
        return model;
    }
}
