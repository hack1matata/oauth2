package com.hackus.learn.resource.server.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientCredentialController {

    @RequestMapping("/test")
    public String test() {
        return "Hello World";
    }
}
