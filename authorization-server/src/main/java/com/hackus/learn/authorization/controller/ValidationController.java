package com.hackus.learn.authorization.controller;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class ValidationController {

    @RequestMapping("/validateUser")
    public Principal user(Principal user) {
        log.info("**************************");
        log.info("**************************");
        log.info("checking user");
        return user;
    }
}
